import java.io.IOException;
import java.util.Scanner;
 
public class Main {
 
    static int[][] MAP = new int[][]{
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
    };
   
   
    static int ballX = 20;
    static int ballY = 7;
    static int ballDx = 1;
    static int ballDy = 0;
    static int bat_1X = 1;
    static int bat_1Y = 7;
    static int bat_2X = 39;
    static int bat_2Y = 7;
     
 
   
 
    public static void main(String args[]) throws IOException {

        Scanner scanner = new Scanner(System.in);
 
        while (true) {
 
            displ();
 
            if (ballX == 0 || ballX == 41) {
                endGame();
            }
 
            int n = scanner.next().charAt(0);
           
            if (n == 'a' || n == 'z' || n == 'q') {
                user_1Input(n);
            }
            if (n == 's' || n == 'x' || n == 'q') {
                user_2Input(n);
            }
 
            moveBall();
        }
    }
 
   
    private static void endGame() throws IOException {
        System.out.println("GAME OVER!!!!");
        System.exit(0);
    }
 
    private static void moveBall() {
        int nextX = ballX - ballDx;
        int nextY = ballY - ballDy;
 
        if (nextY == bat_1Y && nextX == bat_1X ||
            nextY == bat_2Y && nextX == bat_2X) {
            ballDx *= -1;
            ballDy *= -1;
 
        } else {
            ballX = nextX;
            ballY = nextY;
        }
    }
 
   
    private static void user_1Input(int n) {
        switch (n) {
            case 'a':
                if (MAP[bat_1Y - 1][bat_1X] == 0) {
                    bat_1Y -= 1;
                }
                break;
            case 'z':
                if (MAP[bat_1Y + 1][bat_1X] == 0) {
                    bat_1Y += 1;
                }
                break;
            case 'q':
                System.exit(0);
        }
    }
 
    private static void user_2Input(int n) {
        switch (n) {
            case 's':
                if (MAP[bat_2Y - 1][bat_2X] == 0) {
                    bat_2Y -= 1;
                }
                break;
            case 'x':
                if (MAP[bat_2Y + 1][bat_2X] == 0) {
                    bat_2Y += 1;
                }
                break;
            case 'q':
                System.exit(0);
        }
    }
 
    private static void displ() {
        for (int y = 0; y < MAP.length; y++) {
            for (int x = 0; x < MAP[0].length; x++) {
                if (x == ballX && y == ballY) {
                    System.out.print("o");
                }
                else if (y == bat_1Y && x == bat_1X) {
                    System.out.print("|");
                }
                else if (y == bat_2Y && x == bat_2X) {
                    System.out.print("|");
                }
                else if (MAP[y][x] == 1) {
                    System.out.print("!");
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
