package nd2;

import java.io.IOException;

public class Game {

    Ball ball = new Ball();
    Map map = new Map();
    Bat batRight = new Bat(1, (map.getMAP().length)/2);
    Bat batLeft = new Bat((map.getMAP()[0].length)-1, (map.getMAP().length)/2);

    public Game() {
    }

    public void endGame() throws IOException {
        System.out.println("GAME OVER!!!!");
        System.exit(0);

    }

    public void moveBall() {
        int nextX = ball.getBallX() - ball.getBallDx();
        int nextY = ball.getBallY() - ball.getBallDy();

        if (nextY == batLeft.getBatY() && nextX == batLeft.getBatX()
                || nextY == batRight.getBatY() && nextX == batRight.getBatX()) {
            ball.setBallDx(ball.getBallDx() * (-1));
            ball.setBallDy(ball.getBallDy() * (-1));

        } else {
            ball.setBallX(nextX);
            ball.setBallY(nextY);
        }
    }

    public void user_1Input(int n) {
        switch (n) {
            case 'a':
                if (map.getMapElement(batRight.getBatY() - 1, batRight.getBatX()) == 0) {
                    batRight.setBatY(batRight.getBatY() - 1);
                }
                break;
            case 'z':
                if (map.getMapElement(batRight.getBatY() + 1, batRight.getBatX()) == 0) {
                    batRight.setBatY(batRight.getBatY() + 1);
                }
                break;
            case 'q':
                System.exit(0);
        }
    }

    public void user_2Input(int n) {
        switch (n) {
            case 's':
                if (map.getMapElement(batLeft.getBatY() - 1, batLeft.getBatX()) == 0) {
                    batLeft.setBatY(batLeft.getBatY() - 1);
                }
                break;
            case 'x':
                if (map.getMapElement(batLeft.getBatY() + 1, batLeft.getBatX()) == 0) {
                    batLeft.setBatY(batLeft.getBatY() + 1);
                }
                break;
            case 'q':
                System.exit(0);
        }
    }

    public void displ() {
        for (int y = 0; y < map.getMAP().length; y++) {
            for (int x = 0; x < map.getMAP()[0].length; x++) {
                if (x == ball.getBallX() && y == ball.getBallY()) {
                    System.out.print("o");
                } else if (y == batLeft.getBatY() && x == batLeft.getBatX()) {
                    System.out.print("|");
                } else if (y == batRight.getBatY() && x == batRight.getBatX()) {
                    System.out.print("|");
                } else if (map.getMapElement(y, x) == 1) {
                    System.out.print("!");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

}
