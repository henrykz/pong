package nd2;

public class Ball {

    private int ballX;
    private int ballY;
    private int ballDx = 1;
    private int ballDy = 0;
    Map map = new Map();

    public Ball() {
        this.ballX = (map.getMAP()[0].length)/2;
        this.ballY = ValueOfBallY();
    }

    public int ValueOfBallY() {
        int n;
        n = (int) (Math.random() * (map.getMAP().length) + 1);
        return n;
    }

    public void setBallY(int ballY) {
        this.ballY = ballY;
    }

    public void setBallX(int ballX) {
        this.ballX = ballX;
    }

    public void setBallDy(int ballDy) {
        this.ballDy = ballDy;
    }

    public void setBallDx(int ballDx) {
        this.ballDx = ballDx;
    }

    public int getBallX() {
        return ballX;
    }

    public int getBallY() {
        return ballY;
    }

    public int getBallDx() {
        return ballDx;
    }

    public int getBallDy() {
        return ballDy;
    }

}
