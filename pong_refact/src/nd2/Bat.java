package nd2;

public class Bat extends Paddle{

    private int batX;
    private int batY;

    public Bat(int batX, int batY) {
        this.batX = batX;
        this.batY = batY;
    }

    public int getBatX() {
        return batX;
    }

    public void setBatX(int batX) {
        this.batX = batX;
    }

    public int getBatY() {
        return batY;
    }

    public void setBatY(int batY) {
        this.batY = batY;
    }

}
