package nd2;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String args[]) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Game g = new Game();
        while (true) {

            g.displ();

            if (g.ball.getBallX() == 0 || g.ball.getBallX() == 41) {
                g.endGame();
            }

            int n = scanner.next().charAt(0);

            if (n == 'a' || n == 'z' || n == 'q') {
                g.user_1Input(n);
            }
            if (n == 's' || n == 'x' || n == 'q') {
                g.user_2Input(n);
            }

            g.moveBall();
        }
    }

}
